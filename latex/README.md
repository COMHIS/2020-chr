# LaTeX user template and guide

To compile the manuscript ([main.pdf](main.pdf)) use:

1. `pdflatex main`
2. `bibtex main`
3. `pdflatex main`
4. `pdflatex main`

