toprob <- function (logodds) {

  # Odds
  o <- exp(logodds)

  # Probs
  p <- o/(1+o)


  return(p)
  
}

first_year <- function (df) {

  # @description identify the first publication year
  # @details Fields needed: workfield,
  #          first_occurrence, publication_year, actor_name

  # Sort by year
  df <- df %>% arrange(publication_year) %>%
               select(publication_year, workfield)

  # Pick first occurrence for each work
  df <- df[match(unique(df$workfield), df$workfield),]
  firstyear <- as.numeric(df$publication_year)
  names(firstyear) <- as.character(df$workfield)

  firstyear

}
