dfs0$Female <- as.numeric(droplevels(dfs0$gender) == "Female")		   
fit.gender00 <- brm(
            hit | trials(total) ~
                           Female, 
                           family = binomial(),
                           data = dfs0,
			   iter = 2000,
			   chains = 2)

fit.gender2 <- brm(
            hit | trials(total) ~
                           Female + (1 | actor_id), 
                           family = binomial(),
                           data = dfs,
			   #options = list(adapt_delta = 0.9),
			   iter = 2000,
			   chains = 2)

fit.country <- brm(
            hit | trials(total) ~
                           country, 
                           family = binomial(),
                           data = dfs,
			   iter = 2000,
			   chains = 2)

fit.country2 <- brm(
            hit | trials(total) ~
                           (1 | country), 
                           family = binomial(),
                           data = dfs,
			   options = list(adapt_delta = 0.9),			   
			   iter = 2000,
			   chains = 2)

dfs$Female2 <- sample(dfs$Female)
fit.joint1 <- brm(
            hit | trials(total) ~
                           Female + (1 | country),
                           family = binomial(),
                           data = dfs,
			   iter = 2000,
			   chains = 2)

fit.joint2 <- brm(
            hit | trials(total) ~
                           Female + (1 | actor_id) + (1 | country),
                           family = binomial(),
                           data = dfs,
			   control = list(adapt_delta = 0.98),
			   iter = 2000,
			   chains = 2)

fit.joint3 <- brm(
            hit | trials(total) ~
                           Female + country + (1 | actor_id),
                           family = binomial(),
                           data = dfs,
			   control = list(adapt_delta = 0.9),
			   iter = 2000,
			   chains = 2) 


# Fit binomial model based on author activity
library(rstanarm)
fit.joint4 <- stan_glm(cbind(hit, miss) ~ country + gender,
                      data = df,
		      chains = 2,
		      family = binomial(link = "logit"),
		      #prior = student_t(df = 7),
		      #prior_intercept = student_t(df = 7), 
                      prior = normal(0, 2.5, autoscale=FALSE),
		      iter = 1000,
		      warmup = 500,
		      #cores = 4,
		      #control = list(adapt_delta = 0.98),
                      prior_intercept = normal(0, 5, autoscale=FALSE),
                      seed = 12335)

fit.gender0 <- stan_glm(cbind(hit, miss) ~ gender,
                      data = df,
		      chains = 2,
		      family = binomial(link = "logit"),
		      #prior = student_t(df = 7),
		      #prior_intercept = student_t(df = 7), 
                      prior = normal(0, 2.5, autoscale=FALSE),
		      iter = 1000,
		      warmup = 500,
		      #cores = 4,
		      #control = list(adapt_delta = 0.98),
                      prior_intercept = normal(0, 5, autoscale=FALSE),
                      seed = 12335)
		      