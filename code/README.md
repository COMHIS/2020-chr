

### final/

Code for the final manuscript figures is in [final/](final).

**Figure: joint model 2** 
- This gives a model for ECCO coverage based on gender, genre and time window
- (similar example in work/jointmodel.R but without time information; for initial submission)
- Code: [jointmodel2.R](jointmodel2.R)
- Figure: [../output/frequencies.pdf](../output/frequencies.pdf)
- Figure: [../output/logodds.pdf](../output/logodds.pdf)


## work/ 

Various working codes for testing and development purposes are in [work/](work)

**Figure: authors** 
- Matthew effect in ECCO
- Code: [authors.R](authors.R)
- Figure: [../output/authors.png](../output/authors.png)
- Table: [../output/authors.tab](../output/authors.tab)

**Figure: gender** (Leo)
- Matthew effect by gender. 
- Code: [gender.R](gender.R)
- Figure: [../output/gender.png](../output/gender.png)
- Table: [../output/gender.tab](../output/gender.tab)

**Figure: gender and year** (Leo)
- Matthew effect by gender and year. 
- Code: [gender_year.R](gender_year.R)
- Figure: [../output/gender_year.png](../output/gender_year.png)
- Table: [../output/gender_year.tab](../output/gender_year.tab)

**Figure: country** (Leo)
- Matthew effect by country. 
- Code: [country.R](country.R)
- Figure: [../output/country.png](../output/country.png)
- Table: [../output/country.tab](../output/country.tab)

**Figure: genre** (Leo)
- Matthew effect by genre. 
- Code: [genre.R](genre.R)
- Figure: [../output/genre.png](../output/genre.png)
- Table: [../output/genre.tab](../output/genre.tab)


### Data

For data documentation, see the main article on ESTC/ECCO
(article_2020_estc_ecco_survey). The data has been obtained
confidentially for research purposes and cannot be shared openly.
