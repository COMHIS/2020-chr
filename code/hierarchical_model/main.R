
# Read data
#source("read_data.R")     # Done
#source("prepare_data.R")  # Done

source("load_data.R")      # Starting point for the analyses

# Run the model
source("model.R")
