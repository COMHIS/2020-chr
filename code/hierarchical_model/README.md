# Hierarchical model to assess bias in bibliographic data

The [main.R] is the main script that reads the data and runs the analyses.

The [model.R] contains first ready-made examples of simple stan models
for this data. These can be further analysed, visualized, extended, or
replaced with other implementations (rstanarm, brms, stan, etc.).

## Data

The code in main.R assumes that you have access to file "data.Rds".

Full data description:

- gender: author gender
- decade: publishing decade
- author_activity: overall number of works in ESTC for the given author (as categorical variable)
- author_activity_numeric: overall number of works in ESTC for the given author (as numeric variable)
- actor_id: author ID
- sum_estc: number of works in ESTC (for given author and decade)
- sum_ecco: number of works in ECCO (for given author and decade)
- N: number of cases for this combination of variables (always 1 here, not relevant)
- hit: same as sum_ecco
- miss: number of works in ESTC but not covered in ECCO
- hit_freq: coverage (or hit) frequency for ESTC coverage in ECCO
- total: hit + miss (i.e. same as sum_estc)
- Female: logical indicating female author


## Task(s)

ECCO is a subset of ESTC. Various factors, such as author gender,
publication decade, genre, or author activity may affect the coverage
of ESTC works in ECCO ("ECCO coverage").

The task is to model ECCO coverage as a function of selected
background information on the authors and publication.

Investigate the applicability of different probabilistic model
variants in this context.

You could try for instance the following. Starting from the simplest,
and gradually adding more structure. Prepare clear, reproducible
summary reports with Rmarkdown. We can have a discuss the details
already after the first 1-2 models.

It is OK to start by working with a certain decade only, or random
subset of the data in order to speed up.

Models to implement:

 1. ECCO coverage vs. gender (binomial model, already implemented; see model.R)

 1. ECCO coverage vs. gender, with random effect for author (binomial
    model); does the inferred gender effect will change as a result?
    Is there more evidence for one of the models? How about the speed
    of model fitting?

 1. ECCO coverage vs. gender, with random effect for author and
    (random or fixed) effect for decade; does the inferred gender
    effect will change as a result?  Is there more evidence for one of
    the models? How about the speed of model fitting? Note that
    authors are nested in genders but same does not apply for decades.

 1. ECCO coverage vs. total publishing activity of the author as an
    exaplanatory variable (we hypothesize that authors with more
    publications have a higher ECCO coverage; this is the Matthew
    effect). Total publishing activity can be included either as plain
    numeric variable (number of publications; or log10 of it), or as
    an categorical variable (cut logarithmically as this seems to
    work). It is probably best to ignore in the first experiments that
    this is an ordered factor. In fact, you can check some example
    code that does this from [../work/authors.R].

 1. ECCO coverage vs. total publishing activity of the author as an
    exaplanatory variable. Now additionally controlled for gender and
    author (random) effect. Can we observe Matthew effect also after
    controlling for these background factors? How does it change?

It is possible that we get stuck somewhere on the way. In that case,
let us discuss and revise the plan.


We want to understand the following:

 - What are the advantages of probabilistic programming in this
   specific application compared to just counting ECCO coverage
   frequencies for the different groupings of the data (combinations
   of author gender, decade etc) and then (potentially) reaching the
   same qualitative conclusions. Do we get more accurate estimates,
   better fit to the data, notable estimates of uncertainty from the
   stan models..? In other words, show the benefits of the
   probabilistic approach in our application as compared to classical
   or naive approaches. Otherwise it will be hard to justify the use
   of the more complex modeling framework.

 - How well the models scale up with increasing complexity; is model
   inference feasible on a standard laptop?

 - Which models have the most evidence (in model comparison)?

- How much our estimates of the effects (of gender, publication decade
  etc) are changing when we incorporate more information in the
  model; is the gender effect, for instance, just an artifact of
  simultaneous changes publishing activity over time.


## Useful examples

For examples on hierarchical binomial models with stan, see [brms
examples](https://bookdown.org/ajkurz/DBDA_recoded/hierarchical-models.html#speeding-up-jags-brms)
or [brms](https://mc-stan.org/users/interfaces/brms) itself. This is
probably a good start for this application.

Another possibility is to look at
[rstanarm](https://mc-stan.org/rstanarm/articles/rstanarm.html).

You can also try plain [Rstan](https://mc-stan.org/users/interfaces/rstan).


