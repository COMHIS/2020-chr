# Code for CHR2020

[Online Workshop on Computational Humanities Research (18-20 November 2020)](https://www.computational-humanities-research.org)

### Manuscript material

Quantifying bias and uncertainty in historical data collections with
probabilistic programming. Leo Lahti, Eetu Mäkelä, Mikko Tolonen,
2020.

[latex/](latex): LaTeX sources for the manuscript.


### Source code for the experiments

 * [code/](code): source code for the experiments
 * [output/](output): output figures and tables generated for the manuscript




